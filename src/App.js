import { useState } from 'react';
import comma from './components/comma.js'

function App() {
	const [calc, setCalc] = useState("");
	const [result,setResult] = useState("");

	const ops = ['/', '*', '+', '-'];

	const updateCalc = (value) => {

		if (value.includes ("0") && calc === "0" ||
			value.includes ("0") && calc === "."
			){

			return value;
		} 

		if (
			value.includes (".") && value.includes(calc.slice(-1)) ||
			value.includes (".") && calc === (value + ".") 
		){

			return ;
		} 



		
		if (
			
			
			ops.includes(value) && calc === '' ||
			ops.includes(value) && ops.includes(calc.slice(-1))
			 
						
		) {
			return;
		}

		setCalc(calc + value);

		if (!ops.includes(value)) {
			setResult(eval(calc + value).toString());
		}
	}

	const createDigits = () => {
		const digits = [];

		for (let i = 9; i > 0; i--) {
			digits.push(
				<button 
				onClick={() => updateCalc(i.toString())} 
				key={i}>{i}
				</button>
			)
		}

		return digits;
	}

	const calculate = () => {
		setCalc(eval(calc).toString());
	}

	const deleteLast = () => {
		if (calc === '') {
			return;
		}

		const value = calc.slice(0, -1);
		setCalc(value);
	}

	const clearall = () => {
		setCalc('');
        setResult('');
	}

	const negatedigit = () => {
		setCalc(calc *(-1 ) )
	}

	const percents = () => {
		setCalc((eval(calc)) / 100 )
	}


	return (
		<div className="App">
			<div className="calculator">
				<div className="display">
					{result ? <span> </span>: ''}
					{ calc || "0" }
				</div>

				<div className="misc">
					
					<button onClick={clearall}>AC</button>
					<button onClick={negatedigit}>+/-</button>
					<button onClick={percents}>%</button>
					<button onClick={deleteLast}>DEL</button>
				</div>
				<div className="pad">
					<div className="operators">
						<button onClick={() => updateCalc('/')}>/</button>
						<button onClick={() => updateCalc('*')}>*</button>
						<button onClick={() => updateCalc('+')}>+</button>
						<button onClick={() => updateCalc('-')}>-</button>
						<button onClick={calculate}>=</button>
					
					</div>

					<div className="digits">
						{ createDigits() }
						
						<button className="bott" onClick={() => updateCalc('.')}>.</button>
						<button className="bott" onClick={() => updateCalc('0')}>0</button>

						
					</div>

				</div>
				
                    
			</div>
		</div>
	);
}

export default App;
